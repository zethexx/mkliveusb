# Simple Live USB Creator

Create a live UEFI compatible USB device, that can still be used to store files on. Unlike dd or Etcher, this tool copies files to a FAT32 file system allowing the user to keep using the drive as storage.